<?php 

class m_data extends CI_Model{
    
    public function __construct()
    {
        $this->load->database();
    }
    
    // TAMPIL DATA TABLE SYARAT
    public function tampil_data(){
            return $this->db->get('syarat');
    }

    public function get_detail($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('syarat');
        return $query->row_array();
    }
   
    // TAMBAH CALON MHS
    public function t_calonmhs($data,$table){
    $this->db->insert($table,$data);
    }

}