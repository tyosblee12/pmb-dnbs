<?php 

class m_admin extends CI_Model{	

    public function __construct()
    {
        $this->load->database();
    }
    
    // AMBIL DATA CALON MHS
    public function data_mhs(){
        return $this->db->get('calon_mhs');
    }

    public function soft_del($where,$data,$table){
        $this->db->where($where);
        $this->db->update($table,$data);
    }
}