<?php 

class m_mhs extends CI_Model{
    
    public function __construct() {
        $this->load->database(); }
    
    public function tampil_data(){
        return $this->db->get('calon_mhs');
    }
    
    // AWAL CRUD ORANG TUA
    public function tampil_ortu(){
        return $this->db->get_where($table,$where);
    }

    public function t_ortu($data,$table){
        $this->db->insert($table,$data);
    }

    function update_ortu($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}	
    
    function delete_ortu($where,$table){
        $this->db->where($where);
        $this->db->delete($table);
    }
    // AKHIR ORTU
    
    // AWAL CRUD CALON MHS
    function edit_data($where,$table){
        return $this->db->get_where($table,$where);
    }

    function update_data($table,$data,$where){
		// $this->db->where($where);
		$this->db->update($table,$data,$where);
	}	

    function delete_data($where,$table){
        $this->db->where($where);
        $this->db->delete($table);
    }
    // AKHIR CRUD MHS
}