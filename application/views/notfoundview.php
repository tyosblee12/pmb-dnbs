<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="<?php echo base_url('asset/icon.ico');?>">
    <title>Document</title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;700&display=swap" rel="stylesheet">

    <!-- Template CSS -->
    <link rel="stylesheet" href="<?php echo base_url('asset/admin/css/style.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('asset/admin/css/components.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('asset/404.css');?>">
</head>

<body>
    <div id="app">
        <section class="section bg-pan-left">
            <div class="container mt-2">
                <div class="row">
                    <div class="col-12 col-lg-12 col-md-12">
                        <div class="login-brand">
                            <a href="javascript:window.history.go(-1);">
                                <img src="<?php echo base_url('asset/admin/img/DNBS-LOGO.svg');?>" alt="logo"
                                    width="250">
                            </a>
                        </div>
                        <div class="racking-in-expand-fwd-bottom text-center">
                            <h1> <span class="text-blues">404 </span> PAGE NOT FOUND</h1>

                            <a type="button" class="btn btn-primary btn-lg center-blok m-3" tabindex="1"
                                href="javascript:window.history.go(-1);">
                                GO BACK
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <footer class="footer py-3 bg-dark text-muted fixed-bottom">
                <div class="container">
                    <div class="col-md-12 text-center">
                        <span class="text-sm">&copy 2020 - <?php echo date("Y"); ?> Zigma Art Creative Design - All
                            Rights Reserved. | <strong>PMB - STMIK Dharma Negara </strong></span>
                    </div>
                </div>
            </footer> -->
        </section>
    </div>
</body>
<script>
(function titleScroller(text) {
    document.title = text;
    setTimeout(function() {
        titleScroller(text.substr(1) + text.substr(0, 1));
    }, 500);
}(" 404 Page Not Found ! |"));
</script>

</html>