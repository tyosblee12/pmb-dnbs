<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Hi, <?php echo $this->session->userdata('nama'); ?></h1>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                <?php echo $this->session->flashdata('pesanaktif'); ?>
                <div class="card p-3">
                    <div class="card-header">
                        <h4>Udah Data Mahasiswa</h4>
                    </div>
                    <form action="" method="post">
                        <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <label for="id_calonmhs">ID Calon Mahasiswa</label>
                                    <input type="text" class="form-control" id="id_calonmhs" value="" name="id_calonmhs"
                                        readonly>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-12">
                                            <label for="id_prodi">Program Studi</label>
                                            <input type="text" class="form-control" id="id_prodi" value=""
                                                name="id_prodi">
                                        </div>
                                        <div class="col-lg-6 col-md-12">
                                            <label for="kelas">Kelas</label>

                                            <select type="text" class="form-control" id="kelas" name="kelas">
                                                <option> -- Pilih -- </option>
                                                <option value="Reguler">Reguler</option>
                                                <option value="Karyawan">Karyawan</option>
                                                <option value="Weekend">Weekend</option>
                                                <option value="Beasiswa">Beasiswa</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nama">Nama Lengkap</label>
                                    <input type="text" class="form-control" id="nama" value="" name="nama">
                                </div>
                                <div class="form-group">
                                    <label for="status_mhs">Status</label>
                                    <div class="form-check border">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="status_mhs" id="status1"
                                                value="Belum Menikah">
                                            <label class="form-check-label" for="status1">Belum
                                                Menikah</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="status_mhs" id="status2"
                                                value="Menikah">
                                            <label class="form-check-label" for="status2">Menikah</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="tgl_lahir">Tanggal Lahir</label>
                                    <input type="date" class="form-control" id="tgl_lahir" value="" name="tgl_lahir">
                                </div>
                                <div class="form-group">
                                    <label for="tmp_lahir">Tempat Lahir</label>
                                    <input type="text" class="form-control" id="tmp_lahir" name="tmp_lahir" value="">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <label for="no_telp">Nomor Telepon</label>
                                    <input type="text" class="form-control" id="no_telp" value="" name=" no_telp">
                                </div>
                                <div class="form-group">
                                    <label for="alamat">Alamat Lengkap</label>
                                    <textarea type="text" style="height:125px;" class="form-control" id="alamat"
                                        name="alamat" value=""></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="j_kelamin">Jenis Kelamin</label>
                                    <div class="form-check border">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="j_kelamin" id="laki"
                                                value="Laki-Laki">
                                            <label class="form-check-label" for="laki">Laki-Laki</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="j_kelamin" id="Perempuan"
                                                value="Perempuan">
                                            <label class="form-check-label" for="Perempuan">Perempuan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group justify-content-center">
                                    <label for="agama">Agama</label>
                                    <div class="form-check border">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="agama" id="agama1"
                                                value="Islam">
                                            <label class="form-check-label" for="agama1">Islam</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="agama" id="agama2"
                                                value="Katholik">
                                            <label class="form-check-label" for="agama2">Katholik</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="agama" id="agama3"
                                                value="Protestan">
                                            <label class="form-check-label" for="agama3">Protestan</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="agama" id="agama4"
                                                value="Hindu">
                                            <label class="form-check-label" for="agama4">Hindu</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="agama" id="agama5"
                                                value="Budha">
                                            <label class="form-check-label" for="agama5">Budha</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" id="email" value="" name="email">
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-lg text-sm float-right">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
$(document).ready(function() {
    var table = $('#example').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf', 'colvis']
    });

    table.buttons().container()
        .appendTo('#example_wrapper .col-md-6:eq(0)');
});
</script>


<!-- General JS Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="<?php echo base_url('asset/admin/js/stisla.js');?>"></script>

<!-- Template JS File -->
<script src="<?php echo base_url('asset/admin/js/scripts.js');?>"></script>
<script src="<?php echo base_url('asset/admin/js/custom.js');?>"></script>


<!-- Page Specific JS File -->
<script src="<?php echo base_url('asset/admin/js/page/index.js');?>"></script>
</body>

</html>