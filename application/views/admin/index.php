<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Hi, <?php echo $this->session->userdata('nama'); ?></h1>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                <?php echo $this->session->flashdata('pesanhapus'); ?>
                <div class="card p-3">
                    <div class="card-header">
                        <h4>Tabel Calon Mahasiswa</h4>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-bordered table-hover ">
                            <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Kelas</th>
                                    <th scope="col">Nama</th>
                                    <th scope="col">No Telp</th>
                                    <th scope="col">Alamat</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                foreach($calon_mhs as $row)
                                {if($row->is_active == 1){?>
                                <tr>
                                    <td>
                                        <?php echo $row->id_calonmhs ?>
                                    </td>
                                    <td>
                                        <?php echo $row->kelas?>
                                    </td>
                                    <td style="width: auto;">
                                        <?php echo $row->nama; ?>
                                    </td>
                                    <td style="width: auto;">
                                        <a href="https://api.whatsapp.com/send?phone=+62<?php echo $row->no_telp?>&text=Terima Kasih Telah Mendaftar di STMIK Dharma Negara"
                                            class="btn btn-sm btn-success text-white">
                                            <i class="fab fa-whatsapp"></i>
                                        </a> &nbsp <?php echo $row->no_telp ?>
                                    </td>
                                    <td>
                                        <?php echo $row->alamat ?>
                                    </td>
                                    <td>
                                        <a class="btn btn-sm btn-primary text-white"
                                            href="<?php echo base_url(); ?>admin/ubah_mhs/<?php echo $row->id_calonmhs; ?>"><i
                                                class="fas fa-edit"></i></i></a>
                                        <a class="btn btn-sm btn-danger text-white"
                                            href="<?php echo base_url(); ?>admin/soft_delete/<?php echo $row->id_calonmhs; ?>"><i
                                                class="fas fa-trash"></i></i></a>
                                    </td>
                                    <?php }} ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-12 col-sm-12">

            </div>
            <div class="col-lg-6 col-md-12 col-12 col-sm-12">

            </div>
        </div>
    </section>
</div>
<script>
$(document).ready(function() {
    var table = $('#example').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf', 'colvis']
    });

    table.buttons().container()
        .appendTo('#example_wrapper .col-md-6:eq(0)');
});
</script>


<!-- General JS Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="<?php echo base_url('asset/admin/js/stisla.js');?>"></script>

<!-- Template JS File -->
<script src="<?php echo base_url('asset/admin/js/scripts.js');?>"></script>
<script src="<?php echo base_url('asset/admin/js/custom.js');?>"></script>


<!-- Page Specific JS File -->
<script src="<?php echo base_url('asset/admin/js/page/index.js');?>"></script>
</body>

</html>