<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="<?php echo base_url("asset/style.css")?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("asset/mystyle.css") ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;700&display=swap" rel="stylesheet">

    <link rel="icon" href="<?php echo base_url('asset/icon.ico');?>">
    <title>PMB - Dharma Negara</title>
</head>

<body>
    <div class="container">
        <!-- AWAL NAVBAR -->
        <!-- AKHIR NAVBAR -->
        <div class="col-lg-12 col-md-12 text-animate-bottom">
            <div class="pt-5 mt-5 bg-white text-center">
                <div class="container-fluid py-2">
                    <h2 class="font-weight-bold">Pendaftaran</h2>
                    <p>Isi form dibawah ini dengan benar.</p>
                </div>
            </div>
            <?php echo $this->session->flashdata('daftar'); ?>
        </div>
        <div class="container">
            <div class="d-flex justify-content-center w3-animate-bottom">
                <div class="row">
                    <form class="p-3" action="<?php echo base_url(). 'daftar/daftarmhs'; ?>" method="post">
                        <div class="form-group">
                            <label for="id_prodi">Program Studi</label>
                            <input type="text" class="form-control font-weight-bold" id="id_prodi"
                                placeholder="S1 - Teknik Informatika" name="id_prodi" value="S1 - Teknik Informatika"
                                readonly>
                        </div>
                        <!-- <div class="form-group">
                            <label for="id_calonmhs">ID Calon Mahasiswa</label>
                            <input type="text" class="form-control" id="id_calonmhs" placeholder="ID Calon Mahasiswa"
                                name="id_calonmhs" required>
                        </div> -->
                        <div class="form-row border-5 border-success">
                            <div class="form-group col-md-6">
                                <label for="nama">Nama Lengkap</label>
                                <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Lengkap"
                                    required autofocus>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="tgl_lahir">Tanggal Lahir</label>
                                <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="no_telp">Nomor Telepon</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">+62</div>
                                </div>
                                <input type="text" class="form-control" id="no_telp" name="no_telp"
                                    placeholder="Nomor Telepon Aktif" maxlength="13"
                                    onkeypress="return onlyNumberKey(event)" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="alamat">Alamat Lengkap</label>
                            <textarea type="text" class="form-control" id="alamat" name="alamat"
                                placeholder="Komplek Nusa Persada 123" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email" class="form-control" id="email"
                                placeholder="Alamat Email Aktif" required>
                        </div>
                        <div class="form-group">
                            <label for="password">Kata Sandi</label>
                            <input type="password" name="password" class="form-control" id="password"
                                placeholder="******************" required>
                        </div>
                        <button type="submit" class="btn btn-dark bg-blues">Daftar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- AWAL FOOTER -->
    <!-- AKHIR FOOTER -->

    <!-- Optional JavaScript; choose one of the two! -->
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="<?php echo base_url('asset/js/bootsrap-js.js');?>"></script>
    <script>
    function onlyNumberKey(evt) {

        // Only ASCII character in that range allowed
        var ASCIICode = (evt.which) ? evt.which : evt.keyCode
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
            return false;
        return true;
    }
    </script>
</body>

</html>