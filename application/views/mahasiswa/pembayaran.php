<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css"
    rel="stylesheet" />
<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header shadow-lg">
            <h1>Form Pembayaran</h1>
        </div>
        <div class="row">
            <div class="col-6 col-md-6 col-lg-6 mb-4">
                <div class="card border-top border-primary shadow-lg">
                    <div class="card-body">
                        <form action="<?php echo base_url('mahasiswa/update'); ?>" method="post">
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <label for="id_pembayaran">Kode Pembayaran</label>
                                        <input type="text" class="form-control" id="id_pembayaran"
                                            value="<?php echo $this->session->userdata('id_pembayaran'); ?>"
                                            name="id_pembayaran" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="id_calonmhs">ID Calon Mahasiswa</label>
                                        <input type="text" class="form-control" id="id_calonmhs"
                                            value="<?php echo $this->session->userdata('id_calonmhs'); ?>"
                                            name="id_calonmhs" readonly>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <label for="id_prodi">Program Studi</label>
                                                <input type="text" class="form-control" id="id_prodi"
                                                    value="<?php echo $this->session->userdata('id_prodi'); ?>"
                                                    name="id_prodi" readonly>
                                            </div>
                                            <div class="col-lg-6 col-md-12">
                                                <label for="kelas">Kelas</label>
                                                <input type="text"
                                                    value="<?php echo $this->session->userdata('kelas'); ?>"
                                                    class="form-control" id="kelas" name="kelas" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Nama Lengkap</label>
                                        <input type="text" class="form-control" id="nama"
                                            value="<?php echo $this->session->userdata('nama'); ?>" name="nama">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit"
                                            class="btn btn-primary btn-lg text-sm float-right">Cetak</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-6 col-md-6 col-lg-6 mb-4">
                <div class="card border-top border-primary shadow-lg">
                    <div class="card-body">
                        <form action="<?php echo base_url('mahasiswa/update'); ?>" method="post">
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <label for="id_pembayaran">Kode Pembayaran</label>
                                        <input type="text" class="form-control" id="id_pembayaran"
                                            value="<?php echo $this->session->userdata('id_pembayaran'); ?>"
                                            name="id_pembayaran" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="id_calonmhs">ID Calon Mahasiswa</label>
                                        <input type="text" class="form-control" id="id_calonmhs"
                                            value="<?php echo $this->session->userdata('id_calonmhs'); ?>"
                                            name="id_calonmhs" readonly>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <label for="id_prodi">Program Studi</label>
                                                <input type="text" class="form-control" id="id_prodi"
                                                    value="<?php echo $this->session->userdata('id_prodi'); ?>"
                                                    name="id_prodi" readonly>
                                            </div>
                                            <div class="col-lg-6 col-md-12">
                                                <label for="kelas">Kelas</label>
                                                <input type="text"
                                                    value="<?php echo $this->session->userdata('kelas'); ?>"
                                                    class="form-control" id="kelas" name="kelas" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Nama Lengkap</label>
                                        <input type="text" class="form-control" id="nama"
                                            value="<?php echo $this->session->userdata('nama'); ?>" name="nama">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit"
                                            class="btn btn-primary btn-lg text-sm float-right">Cetak</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- <div class="col-12 col-sm-6 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4>Simple</h4>
                    </div>
                    <div class="card-body">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4>Simple</h4>
                    </div>
                    <div class="card-body">
                    </div>
                </div>
            </div> -->
        </div>
    </section>
</div>

<!-- General JS Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="<?php echo base_url('asset/admin/js/stisla.js');?>">
</script>

<!-- Datetime Picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<script>
$("#thn_lulus").datepicker({
    format: "yyyy",
    viewMode: "years",
    minViewMode: "years"
});
</script>

<!-- Template JS File -->
<script src="<?php echo base_url('asset/admin/js/scripts.js');?>"></script>
<script src="<?php echo base_url('asset/admin/js/custom.js');?>"></script>

<!-- Page Specific JS File -->
<!-- <script src="<?php echo base_url('asset/admin/js/page/index.js');?>"></script> -->
</body>

</html>