<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css"
    rel="stylesheet" />
<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header shadow-lg">
            <h1>Form Registrasi</h1>
        </div>
        <?php echo $this->session->flashdata('pesanupdate'); ?>
        <div class="row">
            <div class="col-12 col-md-12 col-lg-12 mb-4">
                <div class="card border-top border-primary shadow-lg">
                    <div class="card-body">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active font-weight-bold" id="diri-tab" data-toggle="tab" href="#diri"
                                    role="tab" aria-controls="diri" aria-selected="true">Data Diri</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link font-weight-bold" id="orangtua-tab" data-toggle="tab"
                                    href="#orangtua" role="tab" aria-controls="orangtua" aria-selected="false">Data
                                    Orang Tua</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link font-weight-bold" id="sekolah-tab" data-toggle="tab" href="#sekolah"
                                    role="tab" aria-controls="sekolah" aria-selected="false">Data Sekolah Asal</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="diri" role="tabpanel" aria-labelledby="diri-tab">
                                <form action="<?php echo base_url('mahasiswa/update'); ?>" method="post"
                                    enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-12">
                                            <div class="form-group">
                                                <label for="id_calonmhs">ID Calon Mahasiswa</label>
                                                <input type="text" class="form-control" id="id_calonmhs"
                                                    value="<?php echo $this->session->userdata('id_calonmhs'); ?>"
                                                    name="id_calonmhs" readonly>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-12">
                                                        <label for="id_prodi">Program Studi</label>
                                                        <input type="text" class="form-control" id="id_prodi"
                                                            value="<?php echo $this->session->userdata('id_prodi'); ?>"
                                                            name="id_prodi">
                                                    </div>
                                                    <div class="col-lg-6 col-md-12">
                                                        <label for="kelas">Kelas</label>
                                                        <?php if ($this->session->userdata('kelas') == "Reguler") {
                                                            $r = "selected";
                                                            $k = "";
                                                            $w = "";
                                                            $b = "";
                                                            $o = "";
                                                        }elseif ($this->session->userdata('kelas') == "Karyawan"){
                                                            $r = "";
                                                            $k = "selected";
                                                            $w = "";
                                                            $b = "";
                                                            $o = "";
                                                        }elseif ($this->session->userdata('kelas') == "Weekend"){
                                                            $r = "";
                                                            $k = "";
                                                            $w = "selected";
                                                            $b = "";
                                                            $o = "";
                                                        }elseif ($this->session->userdata('kelas') == "Beasiswa"){
                                                            $r = "";
                                                            $k = "";
                                                            $w = "";
                                                            $b = "selected";
                                                            $o = "";
                                                        }else{
                                                            $r = "";
                                                            $k = "";
                                                            $w = "";
                                                            $b = "";
                                                            $o = "selected";
                                                        }?>
                                                        <select type="text" class="form-control" id="kelas"
                                                            name="kelas">
                                                            <option <?=$o ?>> -- Pilih -- </option>
                                                            <option value="Reguler" <?=$r ?>>Reguler</option>
                                                            <option value="Karyawan" <?=$k ?>>Karyawan</option>
                                                            <option value="Weekend" <?=$w ?>>Weekend</option>
                                                            <option value="Beasiswa" <?=$b ?>>Beasiswa</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="nama">Nama Lengkap</label>
                                                <input type="text" class="form-control" id="nama"
                                                    value="<?php echo $this->session->userdata('nama'); ?>" name="nama">
                                            </div>
                                            <div class="form-group">
                                                <label for="status_mhs">Status</label>
                                                <?php if ($this->session->userdata('status_mhs') == "Belum Menikah") {
                                                    $bm = "checked";
                                                    $m = "";
                                                }elseif ($this->session->userdata('status_mhs') == "Menikah"){
                                                    $m ="checked";
                                                    $bm = "";
                                                }else{
                                                    $m ="";
                                                    $bm = "";
                                                }?>
                                                <div class="form-check border">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="status_mhs"
                                                            id="status1" value="Belum Menikah" <?= $bm ?>>
                                                        <label class="form-check-label" for="status1">Belum
                                                            Menikah</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="status_mhs"
                                                            id="status2" value="Menikah" <?= $m ?>>
                                                        <label class="form-check-label" for="status2">Menikah</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="tgl_lahir">Tanggal Lahir</label>
                                                <input type="date" class="form-control" id="tgl_lahir"
                                                    value="<?php echo $this->session->userdata('tgl_lahir'); ?>"
                                                    name="tgl_lahir">
                                            </div>
                                            <div class="form-group">
                                                <label for="tmp_lahir">Tempat Lahir</label>
                                                <input type="text" class="form-control" id="tmp_lahir" name="tmp_lahir"
                                                    value="<?php echo $this->session->userdata('tmp_lahir'); ?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12">
                                            <div class="form-group">
                                                <label for="no_telp">Nomor Telepon</label>
                                                <input type="text" class="form-control" id="no_telp"
                                                    value="<?php echo $this->session->userdata('no_telp'); ?>"
                                                    name="no_telp">
                                            </div>
                                            <div class="form-group">
                                                <label for="alamat">Alamat Lengkap</label>
                                                <textarea type="text" style="height:50px;" class="form-control"
                                                    id="alamat" name="alamat"
                                                    value=""><?php echo $this->session->userdata('alamat'); ?></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="j_kelamin">Jenis Kelamin</label>
                                                <?php if ($this->session->userdata('j_kelamin') == "Laki-Laki") {
                                                    $l = "checked";
                                                    $p = "";
                                                }elseif ($this->session->userdata('j_kelamin') == "Perempuan"){
                                                    $p ="checked";
                                                    $l = "";
                                                }else{
                                                    $p ="";
                                                    $l = "";
                                                }?>
                                                <div class="form-check border">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="j_kelamin"
                                                            id="laki" value="Laki-Laki" <?php echo $l; ?>>
                                                        <label class="form-check-label" for="laki">Laki-Laki</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="j_kelamin"
                                                            id="Perempuan" value="Perempuan" <?php echo $p; ?>>
                                                        <label class="form-check-label"
                                                            for="Perempuan">Perempuan</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group justify-content-center">
                                                <label for="agama">Agama</label>
                                                <?php if($this->session->userdata('agama') == "Islam"){
                                                    $i = "checked";       
                                                    $k = "";       
                                                    $p = "";       
                                                    $h = "";       
                                                    $b = "";       
                                                }elseif($this->session->userdata('agama') == "Katholik"){
                                                    $i = "";       
                                                    $k = "checked";       
                                                    $p = "";       
                                                    $h = "";       
                                                    $b = "";   
                                                }elseif($this->session->userdata('agama') == "Protestan"){
                                                    $i = "";       
                                                    $k = "";       
                                                    $p = "checked";       
                                                    $h = "";       
                                                    $b = "";   
                                                }elseif($this->session->userdata('agama') == "Hindu"){
                                                    $i = "";       
                                                    $k = "";       
                                                    $p = "";       
                                                    $h = "checked";       
                                                    $b = "";   
                                                }elseif($this->session->userdata('agama') == "Budha"){
                                                    $i = "";       
                                                    $k = "";       
                                                    $p = "";       
                                                    $h = "";       
                                                    $b = "checked";   
                                                }else{
                                                    $i = "";       
                                                    $k = "";       
                                                    $p = "";       
                                                    $h = "";       
                                                    $b = "";
                                                }?>
                                                <div class="form-check border">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="agama"
                                                            id="agama1" value="Islam" <?php echo $i ?>>
                                                        <label class="form-check-label" for="agama1">Islam</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="agama"
                                                            id="agama2" value="Katholik" <?php echo $k ?>>
                                                        <label class="form-check-label" for="agama2">Katholik</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="agama"
                                                            id="agama3" value="Protestan" <?php echo $p ?>>
                                                        <label class="form-check-label" for="agama3">Protestan</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="agama"
                                                            id="agama4" value="Hindu" <?php echo $h ?>>
                                                        <label class="form-check-label" for="agama4">Hindu</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="agama"
                                                            id="agama5" value="Budha" <?php echo $b ?>>
                                                        <label class="form-check-label" for="agama5">Budha</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="text" class="form-control" id="email"
                                                    value="<?php echo $this->session->userdata('email'); ?>"
                                                    name="email">
                                            </div>
                                            <div class="form-group">
                                                <label for="photo">Photo</label>
                                                <div class="custom-file">
                                                    <input type="file" name="photo" id="photo" class="custom-file-input"
                                                        id="photo" placeholder="choose file">
                                                    <label class="custom-file-label" for="photo">Choose
                                                        file</label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit"
                                            class="btn btn-primary btn-lg text-sm float-right">Update</button>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="orangtua" role="tabpanel" aria-labelledby="orangtua-tab">
                                <form action="<?php echo base_url('mahasiswa/daftarortu'); ?>" method="post">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-12">
                                            <div class="form-group">
                                                <label for="id_calonmhs">ID Calon Mahasiswa</label>
                                                <input type="text" class="form-control" id="id_calonmhs"
                                                    value="<?php echo $this->session->userdata('id_calonmhs'); ?>"
                                                    name="id_calonmhs" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label for="nama">Nama Orang Tua / Wali</label>
                                                <input type="text" class="form-control" id="nama" value="" name="nama">
                                            </div>
                                            <div class="form-group">
                                                <label for="no_telp">Nomor Telepon</label>
                                                <input type="text" class="form-control" id="no_telp" value=""
                                                    name="no_telp">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12">
                                            <div class="form-group">
                                                <label for="pekerjaan">Pekerjaan</label>
                                                <select type="text" class="form-control" id="pekerjaan"
                                                    name="pekerjaan">
                                                    <option value="PNS">PNS</option>
                                                    <option value="TNI/Polri">TNI/Polri</option>
                                                    <option value="Guru/Dosen">Guru/Dosen</option>
                                                    <option value="Pensiunan">Pensiunan</option>
                                                    <option value="Petani/Buruh">Petani/Buruh</option>
                                                    <option value="Swasta">Swasta</option>
                                                    <option value="Wirausaha">Wirausaha</option>
                                                    <option value="Lain-lain">Lain-lain</option>
                                                    <option selected>-- Pilih --</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="instansi">Nama Instansi</label>
                                                <input type="text" class="form-control" id="instansi" name="instansi">
                                            </div>
                                            <div class="form-group">
                                                <label for="pend_terakhir">Pendidikan Terakhir</label>
                                                <select type="text" class="form-control" id="pend_terakhir"
                                                    name="pend_terakhir">
                                                    <option value="S3">S3</option>
                                                    <option value="S2">S2</option>
                                                    <option value="S1">S1</option>
                                                    <option value="Diploma">Diploma</option>
                                                    <option value="SLTA">SLTA</option>
                                                    <option selected>-- Pilih --</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit"
                                            class="btn btn-primary btn-lg text-sm float-right">Update</button>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="sekolah" role="tabpanel" aria-labelledby="sekolah-tab">
                                <form action="#" method="post">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-12">
                                            <div class="form-group">
                                                <label for="id_calonmhs">ID Calon Mahasiswa</label>
                                                <input type="text" class="form-control" id="id_calonmhs"
                                                    value="<?php echo $this->session->userdata('id_calonmhs'); ?>"
                                                    name="id_calonmhs" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label for="asal_sekolah">Asal Sekolah</label>
                                                <input type="text" class="form-control" id="asal_sekolah" value=""
                                                    name="asal_sekolah">
                                            </div>
                                            <div class="form-group">
                                                <label for="jurusan">Jurusan</label>
                                                <input type="text" class="form-control" id="jurusan" value=""
                                                    name="jurusan">
                                            </div>
                                            <div class="form-group">
                                                <label for="nm_sekolah">Nama Sekolah</label>
                                                <input type="text" class="form-control" id="nm_sekolah" value=""
                                                    name="nm_sekolah">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12">
                                            <div class="form-group">
                                                <label for="thn_lulus">Tahun Lulus</label>
                                                <input type="text" class="form-control" id="thn_lulus" name="thn_lulus">
                                            </div>
                                            <div class="form-group">
                                                <label for="no_ijazah">Nomor Ijazah</label>
                                                <input type="text" class="form-control" id="no_ijazah" name="no_ijazah">
                                            </div>
                                            <div class="form-group">
                                                <label for="alamat">Alamat Sekolah</label>
                                                <textarea type="text" style="height:150px;" class="form-control"
                                                    id="alamat" name="alamat">
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="form-group">
                                    <button type="submit"
                                        class="btn btn-primary btn-lg text-sm float-right">Update</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- <div class="col-12 col-sm-6 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4>Simple</h4>
                    </div>
                    <div class="card-body">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4>Simple</h4>
                    </div>
                    <div class="card-body">
                    </div>
                </div>
            </div> -->
        </div>
    </section>
</div>

<!-- General JS Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="<?php echo base_url('asset/admin/js/stisla.js');?>">
</script>

<!-- Datetime Picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<script>
$("#thn_lulus").datepicker({
    format: "yyyy",
    viewMode: "years",
    minViewMode: "years"
});
</script>

<!-- Template JS File -->
<script src="<?php echo base_url('asset/admin/js/scripts.js');?>"></script>
<script src="<?php echo base_url('asset/admin/js/custom.js');?>"></script>

<!-- Page Specific JS File -->
<!-- <script src="<?php echo base_url('asset/admin/js/page/index.js');?>"></script> -->
</body>

</html>