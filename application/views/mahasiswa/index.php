<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Selamat Datang</h1>
        </div>
        <?php echo $this->session->flashdata('pesan'); ?>
        <div class="row">
            <div class="col-12 col-md-12 col-lg-12 mb-4">
                <div class="hero text-white hero-bg-image hero-bg-parallax"
                    data-background="<?php echo base_url('asset/img/bg.jpg') ;?>">
                    <div class="hero-inner">
                        <h2>Hallo, <?php echo $this->session->userdata('nama'); ?></h2>
                        <p class="lead">Anda hampir sampai, lengkapi informasi tentang akun Anda sampai selesai
                            Registrasi.</p>
                        <div class="mt-4">
                            <a href="<?php echo base_url('mahasiswa/data_diri'); ?>"
                                class="btn btn-outline-white btn-lg btn-icon icon-left"><i class="far fa-user"></i>
                                Lengkapi Data</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- General JS Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="<?php echo base_url('asset/admin/js/stisla.js');?>"></script>

<!-- Template JS File -->
<script src="<?php echo base_url('asset/admin/js/scripts.js');?>"></script>
<script src="<?php echo base_url('asset/admin/js/custom.js');?>"></script>

<!-- Page Specific JS File -->
<script src="<?php echo base_url('asset/admin/js/page/index.js');?>"></script>

</body>

</html>