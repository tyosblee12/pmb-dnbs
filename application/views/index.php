<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"> -->
    <link rel="icon" href="<?php echo base_url('asset/icon.ico');?>">
    <link rel="stylesheet" href="<?php echo base_url("asset/style.css")?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("asset/mystyle.css") ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;700&display=swap" rel="stylesheet">
    <title>PMB - Dharma Negara</title>
</head>

<body>
    <!-- AWAL NAVBAR -->
    <!-- AKHIR NAVBAR -->
    <div class="col-lg-12 col-md-12 text-animate-bottom">
        <div class="pt-5 mt-5 bg-white text-center">
            <div class="container-fluid py-5">
                <h1 class="font-weight-bold">Selamat Datang </br>
                    Calon Mahasiswa <span class="text-blues">STMIK Dharma Negara</span></h1>
                <h5 class="p-2">Silakan pilih jenjang pendidikan yang ingin ditempuh</h5>
            </div>
        </div>
    </div>
    <div class="container">
        <script type="text/javascript">
        atOptions = {
            'key': '1b6c5570d95db3c4afbc489763762da2',
            'format': 'iframe',
            'height': 90,
            'width': 728,
            'params': {}
        };
        document.write('<scr' + 'ipt type="text/javascript" src="http' + (location.protocol === 'https:' ? 's' : '') +
            '://www.variouscreativeformats.com/1b6c5570d95db3c4afbc489763762da2/invoke.js"></scr' + 'ipt>');
        </script>
        <div class="row justify-content-around">
            <div class="card-deck pr-3 pl-3">
                <a class="card shadow-sm card-link text-dark card-4 m-2" style="width: 24rem;"
                    href="<?php echo base_url('daftar');?>">
                    <div class="card-body">
                        <h1 class="card-title font-weight-bold">S1</h1>
                        <h5 class="card-subtitle mb-2 text-muted">Teknik </br>Informatika</h5>
                        <h6 class="card-text">Mahasiswa Baru</h6>
                    </div>
                </a>
                <a class="card shadow-sm card-link text-dark card-6 m-2" style="width: 24rem;" href="#">
                    <div class="card-body">
                        <h1 class="card-title font-weight-bold">S1</h1>
                        <h5 class="card-subtitle mb-2 text-muted">Teknik </br>Informatika</h5>
                        <h6 class="card-text">Mahasiswa Pindahan</h6>
                    </div>
                </a>
            </div>
        </div>
    </div>


    <!-- AWAL FOOTER -->
    <!-- AKHIR FOOTER -->

    <!-- Optional JavaScript; choose one of the two! -->
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script> -->
    <script src="<?php echo base_url('asset/js/bootsrap-js.js');?>"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.6.1/gsap.min.js"></script>
    <script>
    gsap.from('.navbar', {
        duration: 1.5,
        y: '-100%',
        opacity: 0,
        ease: 'bounce'
    });
    gsap.from('.container-fluid h1 , h5', {
        duration: 1.5,
        delay: 1,
        y: '-100%',
        opacity: 0,
        ease: 'elastic.out(1, 0.3)'
    })
    </script>

</body>

</html>