<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class master extends CI_Controller {

    function __construct(){
		parent::__construct();		
		$this->load->model('m_login');
	}

    public function index(){
        $this->load->view('admin/login');
    }
    
    
    function LoginActionAdmin(){
	
		$username = $this->input->post('username', TRUE);
		$password = md5($this->input->post('password', TRUE));

		$cek = $this->m_login->admin_login("admin", array ('username' => $username), array ('password' => $password));
        
        if($cek != FALSE){
			foreach ($cek as $detail) {
				$data_session = array(
					'id_admin' => $detail->id_admin,
					'nama' => $detail->nama,
					'username' => $detail->username,
					'status' => "admin"
					);
				$this->session->set_userdata($data_session);
				$this->session->set_flashdata(
					'pesan',
					'<div class="alert alert-info alert-dismissible fade show" role="alert">
					<strong>Terima Kasih Telah Mendaftar</strong>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				  </div>'
				);
				redirect(base_url('admin'));
			}
			
			}else{
				
			echo '<script language="javascript">';
			echo 'alert("message successfully sent")';
			echo '</script>';
			redirect(base_url('master'));
			exit();
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url('master'));
	}

}