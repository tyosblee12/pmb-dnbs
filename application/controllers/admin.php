<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class admin extends CI_Controller {
	
	function __construct(){
		parent::__construct();		
		$this->load->model('m_login');
		$this->load->model('m_admin');
		if($this->session->userdata('status') != "admin"){
			redirect(base_url('master'));
		}
	}
	
	public function index()
	{		
		$data['calon_mhs'] = $this->m_admin->data_mhs()->result();
		$this->load->view('admin/template/headeradmin');
		$this->load->view('admin/index',$data);
		$this->load->view('admin/template/footeradmin');
	}
	
	public function alldata()
	{		
		$data['calon_mhs'] = $this->m_admin->data_mhs()->result();
		$this->load->view('admin/template/headeradmin');
		$this->load->view('admin/all_data',$data);
		$this->load->view('admin/template/footeradmin');
	}

	public function soft_delete($id_calonmhs){
		$where = array('id_calonmhs' => $id_calonmhs);
		$data = array('is_active' => 0);

		$this->db->update('calon_mhs', $data, $where);
		$this->session->set_flashdata(
			'pesanhapus',
			'<div class="alert alert-info alert-dismissible fade show" role="alert">
			<strong>Data Berhasil di hapus</strong>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>');
		redirect('admin');
	}

	public function aktifkan($id_calonmhs){
		$where = array('id_calonmhs' => $id_calonmhs);
		$data = array('is_active' => 1);

		$this->db->update('calon_mhs', $data, $where);
		$this->session->set_flashdata(
			'pesanaktif',
			'<div class="alert alert-info alert-dismissible fade show" role="alert">
			<strong>Mahasiswa Telah diaktifkan kembali</strong>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    		<span aria-hidden="true">&times;</span>
			</button>
			</div>');
		redirect('admin/alldata');
}
	
		public function ubah_mhs($id_calonmhs){
			
			$where = array('id_calonmhs' => $id_calonmhs);

			$this->load->view('admin/template/headeradmin');
			$this->load->view('admin/edit_mhs');
			$this->load->view('admin/template/footeradmin');
			
		// 	$data = array(
		// 		'id_prodi' => $id_prodi,
		// 		'kelas' => $kelas,
		// 		'nama' => $nama,
		// 		'j_kelamin' => $j_kelamin,
		// 		'agama' => $agama,
		// 		'status_mhs' => $status_mhs,
		// 		'tgl_lahir' => $tgl_lahir,
		// 		'tmp_lahir' => $tmp_lahir,
		// 		'no_telp' => $no_telp,
		// 		'alamat' => $alamat,
		// 		'email' => $email,
		// );
}

}