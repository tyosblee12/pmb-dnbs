<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class utama extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('m_data');
        $this->load->helper('url');
	}

	public function index()
	{
		$data['syarat'] = $this->m_data->tampil_data()->result();
		$this->load->view('template/header' , $data);
		$this->load->view('index');
		$this->load->view('template/footer');
		// var_dump($data);
	}
	

}