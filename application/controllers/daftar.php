<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class daftar extends CI_Controller {
	
	function __construct(){
		parent::__construct();		
		$this->load->model('m_data');
        $this->load->helper('url');
		$this->load->library('form_validation');
	}
	
	public function index()
	{
		$data['syarat'] = $this->m_data->tampil_data()->result();
		$this->load->view('daftar/daftar');
		$this->load->view('template/footer');	
		$this->load->view('template/header',$data);
	}	
	
	public function daftarmhs()
	{
			// AMBIL KODE ID CALON TERAKHIR 
			$get_id = $this->db->order_by('id_calonmhs',"desc")
			->select('id_calonmhs')
			->limit(1)
			->get('calon_mhs')
			->row();
			$id = $get_id->id_calonmhs + 1;

			// AMBIL DATA DARI FORM DAFTAR
			$id_calonmhs = $id;
			$id_prodi = $this->input->post('id_prodi');
			$nama = $this->input->post('nama');
			$tgl_lahir = $this->input->post('tgl_lahir');
			$no_telp = $this->input->post('no_telp');
			$alamat = $this->input->post('alamat');
			$email = $this->input->post('email');
			$password = md5($this->input->post('password'));
	
			$data = array(
				'id_calonmhs' => $id_calonmhs,
				'id_prodi' => $id_prodi,
				'nama' => $nama,
				'tgl_lahir' => $tgl_lahir,
				'no_telp' => $no_telp,
				'alamat' => $alamat,
				'is_active' => '1',
				'email' => $email,
				'password' => $password
				);
				
			$this->m_data->t_calonmhs($data,'calon_mhs');
			$this->session->set_flashdata(
				'daftar',
				'<div class="alert alert-info alert-dismissible fade show" role="alert">
				<strong>Silahkan Login untuk melengkapi data</strong>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>'
			);
			redirect('daftar/index');
	}
}