<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mahasiswa extends CI_Controller {

	function __construct(){
		parent::__construct();
			$this->load->model('m_mhs');
			if($this->session->userdata('status') != "login"){
			redirect(base_url('login'));
		}
	}

	
	public function index()
	{
		$data['data_diri'] = " ";
		$data['awal'] = "active";
		$data['profil'] = " ";
		$data['bayar'] = " ";
		$this->load->view('mahasiswa/template/headermhs',$data);
		$this->load->view('mahasiswa/index');
		$this->load->view('mahasiswa/template/footermhs');
	}
	
	public function data_diri()
	{
		$data['data_diri'] = "active";
		$data['awal'] = " ";
		$data['profil'] = " ";
		$data['bayar'] = "";
		$this->load->view('mahasiswa/template/headermhs', $data);
		$this->load->view('mahasiswa/form_daftar');
		$this->load->view('mahasiswa/template/footermhs');
	}

	public function profil()
	{
		$data['data_diri'] = "";
		$data['awal'] = "";
		$data['profil'] = "active";
		$data['bayar'] = "";
		$this->load->view('mahasiswa/template/headermhs', $data);
		$this->load->view('mahasiswa/profil');
		$this->load->view('mahasiswa/template/footermhs');
	}
	
	public function pembayaran()
	{
		$data['data_diri'] = "";
		$data['awal'] = "";
		$data['profil'] = "";
		$data['bayar'] = "active";
		$this->load->view('mahasiswa/template/headermhs', $data);
		$this->load->view('mahasiswa/pembayaran');
		$this->load->view('mahasiswa/template/footermhs');
	}

	public function update(){
		
		$id_calonmhs = $this->input->post('id_calonmhs');
		$id_prodi = $this->input->post('id_prodi');
		$kelas = $this->input->post('kelas');
		$nama = $this->input->post('nama');
		$j_kelamin = $this->input->post('j_kelamin');
		$agama = $this->input->post('agama');
		$status_mhs = $this->input->post('status_mhs');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$tmp_lahir = $this->input->post('tmp_lahir');
		$no_telp = $this->input->post('no_telp');
		$alamat = $this->input->post('alamat');
		$email = $this->input->post('email');
		$password = md5($this->input->post('password'));

		$photo = $_FILES['photo']['name'];
		if ($photo){
			$config['upload_path'] = './asset/img/upload';
			$config['allowed_types'] = 'jpeg|jpg|png';

			$this->load->library('upload', $config);
			if($this->upload->do_upload('photo')){
				$photo = $this->upload->data('file_name');
				$this->db->set('photo', $photo);
			}else{
				echo "Photo Gagal Upload";
			}
		}
		
		$data = array(
				'id_prodi' => $id_prodi,
				'kelas' => $kelas,
				'nama' => $nama,
				'j_kelamin' => $j_kelamin,
				'agama' => $agama,
				'status_mhs' => $status_mhs,
				'tgl_lahir' => $tgl_lahir,
				'tmp_lahir' => $tmp_lahir,
				'no_telp' => $no_telp,
				'alamat' => $alamat,
				'email' => $email,
				'photo' => $photo
		);

		$where = array(
			'id_calonmhs' => $id_calonmhs
		);

		$this->m_mhs->update_data('calon_mhs', $data, $where);
		
		if($this->session->userdata('status') == "login"){
			$this->session->set_flashdata(
				'pesanupdate',
				'<div class="alert alert-info alert-dismissible fade show" role="alert">
				<strong>Data Berhasil di Update</strong>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>'
			);
			redirect('mahasiswa/data_diri');
		}
	}


	public function daftarortu()
	{
			// AMBIL KODE ID CALON TERAKHIR 
			// $get_id = $this->db->order_by('id_calonmhs',"desc")
			// ->select('id_calonmhs')
			// ->limit(1)
			// ->get('calon_mhs')
			// ->row();
			// $id = $get_id->id_calonmhs + 1;

			// AMBIL DATA DARI FORM DAFTAR
			
			$id_calonmhs = $this->input->post('id_calonmhs');
			$nama = $this->input->post('nama');
			$no_telp = $this->input->post('no_telp');
			$pekerjaan = $this->input->post('pekerjaan');
			$instansi = $this->input->post('instansi');
			$pend_terakhir = $this->input->post('pend_terakhir');
	
			$data = array(
				'id_calonmhs' => $id_calonmhs,
				'nama' => $nama,
				'no_telp' => $no_telp,
				'pekerjaan' => $pekerjaan,
				'instansi' => $instansi,
				'pend_terakhir' => $pend_terakhir,
				);
				
			$this->m_mhs->t_ortu($data,'orangtua_mhs');
			redirect('mahasiswa/data_diri');
	}
	
	// AMBIL DATA WHERE SESSION


}