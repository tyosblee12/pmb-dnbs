<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class login extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('m_login');
	}

	public function index()
	{
		$this->load->view('login');	
	}

	function LoginAction(){
	
		$email = $this->input->post('email', TRUE);
		$password = md5($this->input->post('password', TRUE));
		
		$cek = $this->m_login->cek_login("calon_mhs", array ('email' => $email), array ('password' => $password));
		if($cek != FALSE){
			foreach ($cek as $detail) {
				$data_session = array(
					'id_calonmhs' => $detail->id_calonmhs,
					'id_prodi' => $detail->id_prodi,
					'kelas' => $detail->kelas,
					'nama' => $detail->nama,
					'j_kelamin' => $detail->j_kelamin,
					'agama' => $detail->agama,
					'status_mhs' => $detail->status_mhs,
					'tgl_lahir' => $detail->tgl_lahir,
					'tmp_lahir' => $detail->tmp_lahir,
					'alamat' => $detail->alamat,
					'no_telp' => $detail->no_telp,
					'email' => $detail->email,
					'status' => "login"
					);
				$this->session->set_userdata($data_session);
				$this->session->set_flashdata(
					'pesan',
					'<div class="alert alert-info alert-dismissible fade show" role="alert">
					<strong>Terima Kasih Telah Mendaftar</strong>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				  </div>'
				);
				redirect(base_url('mahasiswa'));
			}
			
			}else{
				
			echo '<script language="javascript">';
			echo 'alert("message successfully sent")';
			echo '</script>';
			redirect(base_url('login'));
			exit();
		}
	}

	public function logout(){
		$this->session->set_flashdata(
			'logout',
			'<div class="alert alert-info alert-dismissible fade show" role="alert">
			<strong>Anda telah logout</strong>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>'
		);
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}

}