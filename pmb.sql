-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 02, 2021 at 08:01 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pmb`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` tinyint(128) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `nama`, `username`, `password`) VALUES
(1, 'Setyo Dwi Cahyo', 'admin@email.com', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `asalsklh_mhs`
--

CREATE TABLE `asalsklh_mhs` (
  `id_calonmhs` int(15) NOT NULL,
  `asal_sklh` varchar(128) NOT NULL,
  `jurusan` varchar(128) NOT NULL,
  `nama_sklh` varchar(128) NOT NULL,
  `tahun_lulus` date NOT NULL,
  `no_ijazah` tinyint(128) NOT NULL,
  `photo_ijazah` varchar(50) NOT NULL,
  `alamat_sekolah` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `calon_mhs`
--

CREATE TABLE `calon_mhs` (
  `id_calonmhs` int(15) NOT NULL,
  `id_prodi` varchar(128) NOT NULL,
  `kelas` varchar(15) DEFAULT NULL,
  `nama` varchar(50) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `alamat` text NOT NULL,
  `tmp_lahir` varchar(128) DEFAULT NULL,
  `j_kelamin` varchar(128) DEFAULT NULL,
  `agama` varchar(128) DEFAULT NULL,
  `status_mhs` varchar(128) DEFAULT NULL,
  `is_active` tinyint(2) NOT NULL,
  `photo` varchar(128) DEFAULT NULL,
  `email` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `calon_mhs`
--

INSERT INTO `calon_mhs` (`id_calonmhs`, `id_prodi`, `kelas`, `nama`, `tgl_lahir`, `no_telp`, `alamat`, `tmp_lahir`, `j_kelamin`, `agama`, `status_mhs`, `is_active`, `photo`, `email`, `password`) VALUES
(1272101032, 'S1 - Teknik Informatika', 'Reguler', 'Saila Saadiah Amanatillah', '1999-07-24', '83821089105', 'Bandung', NULL, NULL, NULL, NULL, 1, NULL, 'sailasa@mail.com', '750760264859afbe7286bdc17391d2d5'),
(1272101035, 'S1 - Teknik Informatika', 'Beasiswa', 'Septianus Cahyadi', '2021-04-22', '123123123', 'Cianjur', NULL, NULL, NULL, NULL, 1, NULL, 'septi@mail.com', '21232f297a57a5a743894a0e4a801fc3'),
(1272101036, 'S1 - Teknik Informatika', 'Weekend', 'Rizky Hidayat Rahman', '2021-04-27', '82445588991', ' Tasikmalaya ', 'Cimahi', 'Laki-Laki', 'Islam', 'Belum Menikah', 1, NULL, 'rizkyhidayat@gmail.com', '653ff4578b4af72248b69af95d8c9b1e'),
(1272101037, 'S1 - Teknik Informatika', 'Beasiswa', 'Rayci Muhcidfudin', '2021-04-27', '82455882288', ' Cimahi ', 'Cimahi', 'Laki-Laki', 'Islam', 'Belum Menikah', 1, NULL, 'rayci@gmail.com', '653ff4578b4af72248b69af95d8c9b1e'),
(1272101038, 'S1 - Teknik Informatika', NULL, 'Randy yohanes', '0000-00-00', '83214455789', 'Bandung', NULL, NULL, NULL, NULL, 1, NULL, 'randyy@gmail.com', '653ff4578b4af72248b69af95d8c9b1e'),
(1272101039, 'S1 - Teknik Informatika', NULL, 'Aldi Wijaya Yusuf', '2021-04-15', '86245533159', 'Garut', NULL, NULL, NULL, NULL, 1, NULL, 'aldiwy@gmail.com', '653ff4578b4af72248b69af95d8c9b1e'),
(1272101040, 'S1 - Teknik Informatika', 'Reguler', 'Sandy Akhmad Ramdiansyah', '2021-04-21', '82365544782', 'Cimahi', 'Cimahi', 'Laki-Laki', 'Islam', 'Belum Menikah', 1, NULL, 'sandy@gmail.com', '653ff4578b4af72248b69af95d8c9b1e'),
(1272101041, 'S1 - Teknik Informatika', 'Reguler', 'Setyo Dwi Cahyo', '1996-11-03', '85624400593', 'Bandung', 'Cimahi', 'Laki-Laki', 'Islam', 'Belum Menikah', 1, NULL, 'tyosblee@hotmail.com', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `orangtua_mhs`
--

CREATE TABLE `orangtua_mhs` (
  `id` tinyint(4) NOT NULL,
  `id_calonmhs` int(15) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `no_telp` varchar(128) NOT NULL,
  `pekerjaan` varchar(128) NOT NULL,
  `instansi` varchar(128) NOT NULL,
  `pend_terakhir` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `syarat`
--

CREATE TABLE `syarat` (
  `id` smallint(5) NOT NULL,
  `konten` text NOT NULL,
  `status` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `syarat`
--

INSERT INTO `syarat` (`id`, `konten`, `status`) VALUES
(1, 'Lulusan SMU / Kejuruan Semua Jurusan', '1'),
(2, 'Biaya Pendaftaran ( Sarjana & Diploma III) Rp. 350.000,-', '1'),
(3, 'Biaya Pendaftaran ( Pasca Sarajana ) Rp. 500.000,-', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `asalsklh_mhs`
--
ALTER TABLE `asalsklh_mhs`
  ADD KEY `id_calonmhs` (`id_calonmhs`);

--
-- Indexes for table `calon_mhs`
--
ALTER TABLE `calon_mhs`
  ADD PRIMARY KEY (`id_calonmhs`);

--
-- Indexes for table `orangtua_mhs`
--
ALTER TABLE `orangtua_mhs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_calonmhs` (`id_calonmhs`);

--
-- Indexes for table `syarat`
--
ALTER TABLE `syarat`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `orangtua_mhs`
--
ALTER TABLE `orangtua_mhs`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `asalsklh_mhs`
--
ALTER TABLE `asalsklh_mhs`
  ADD CONSTRAINT `asalsklh_mhs_ibfk_1` FOREIGN KEY (`id_calonmhs`) REFERENCES `calon_mhs` (`id_calonmhs`);

--
-- Constraints for table `orangtua_mhs`
--
ALTER TABLE `orangtua_mhs`
  ADD CONSTRAINT `orangtua_mhs_ibfk_1` FOREIGN KEY (`id_calonmhs`) REFERENCES `calon_mhs` (`id_calonmhs`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
